var indexSectionsWithContent =
{
  0: "acdegijmnoprstuvw~",
  1: "acdgjmopstv",
  2: "i",
  3: "cijmoprstv",
  4: "cgjmopstuvw~",
  5: "adegijmnopsuvw",
  6: "o",
  7: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "related",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Friends",
  7: "Pages"
};

