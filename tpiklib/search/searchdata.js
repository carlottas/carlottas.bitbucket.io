var indexSectionsWithContent =
{
  0: "abcdefghijlopqrstuvwxy~",
  1: "abceipst",
  2: "t",
  3: "aceiprst",
  4: "acefgiprstuw~",
  5: "abcdghijlopqrstvxy",
  6: "h",
  7: "o",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "related",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Friends",
  8: "Pages"
};

